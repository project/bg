��          �            x     y     �  &   �  G   �  -     &   F  &   m  !   �  "   �  -   �  %        -  (   L  )   u     �  �  �  8   �  7   �  s   �  �   n  n     \   u  ^   �  V   1  Z   �  G   �  J   +	  P   v	  Q   �	  V   
  9   p
                                                  	               
              Created directory %directory. File copy failed. File copy failed. File already exists. File copy failed: no directory configured, or it could not be accessed. File copy failed: source file does not exist. File upload failed: file size too big. File upload failed: incomplete upload. File upload failed: invalid data. File upload failed: unknown error. Modified permissions on directory %directory. Possible exploit abuse: invalid data. Removing original file failed. The directory %directory does not exist. The directory %directory is not writable. Unable to create file. Project-Id-Version: Drupal Bulgarian Translation
POT-Creation-Date: 2004-11-29 16:04+0000
PO-Revision-Date: 2004-12-30 13:34+0100
Last-Translator: Daniel Duraliyski <daniel.duraliyski@animacenter.net>
Language-Team: Bulgarian <bgdrupal@starbox.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Bulgarian
X-Poedit-Country: BULGARIA
X-Poedit-SourceCharset: utf-8
 Създаване на директория %directory. Копирането на фаил се провали. Копирането завърши неуспешно. Файл с това име вече съществува.  Копирането на файла се провали: не е конфигурирана директория или тя не е достъпна. Копирането на фаил се провали: изходният фаил не съществува. Качването на фила се провали: файла е твърде голям. Качването на файла се провали: незавършено качване. Качването на фаила се провали: невалидни данни. Качването на фаила се провали: неизвестна грешка. Променяне правата на директория %directory. Възможна е злоупотреба: невалидни данни. Изтриването на оригиналния фаил се провали. Директорията %directory не може да бъде намерена. В директорията %directory не може да бъде записвано. Файлът не може да бъде създаден 